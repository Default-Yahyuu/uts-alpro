import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class DoWhilePembayaran {
    public static void main(String[] args) throws IOException {
        int sksPerSemester = 24;
        int biayaPerSKS = 75000;
        int dendaPerSKS = 5000;

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int jumlahSKS;

        do {
            System.out.print("Masukkan jumlah mata kuliah yang Anda ambil: ");
            jumlahSKS = Integer.parseInt(reader.readLine());
        } while (jumlahSKS <= 0);

        int totalBiayaKuliah = jumlahSKS * biayaPerSKS;
        int biayaDenda = (jumlahSKS > sksPerSemester) ? ((jumlahSKS - sksPerSemester) * dendaPerSKS) : 0;
        int totalBiayaTermasukDenda = totalBiayaKuliah + biayaDenda;

        System.out.println("Total biaya kuliah Anda (termasuk biaya denda): Rp " + totalBiayaTermasukDenda);
    }
}
