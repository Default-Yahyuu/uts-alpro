import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class GetSetPembayaran {
    private int sksPerSemester;
    private int biayaPerSKS;
    private int dendaPerSKS;

    public GetSetPembayaran(int sksPerSemester, int biayaPerSKS, int dendaPerSKS) {
        this.sksPerSemester = sksPerSemester;
        this.biayaPerSKS = biayaPerSKS;
        this.dendaPerSKS = dendaPerSKS;
    }

    public int hitungTotalBiaya(int jumlahSKS) {
        int totalBiayaKuliah = jumlahSKS * biayaPerSKS;
        int biayaDenda = (jumlahSKS > sksPerSemester) ? ((jumlahSKS - sksPerSemester) * dendaPerSKS) : 0;
        return totalBiayaKuliah + biayaDenda;
    }

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Masukkan jumlah mata kuliah yang Anda ambil: ");
        int jumlahSKS = Integer.parseInt(reader.readLine());

        GetSetPembayaran pembayaran = new GetSetPembayaran(24, 75000, 5000);
        int totalBiaya = pembayaran.hitungTotalBiaya(jumlahSKS);

        System.out.println("Total biaya kuliah Anda (termasuk biaya denda): Rp " + totalBiaya);
    }
}
