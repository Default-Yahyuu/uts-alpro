import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class IfElsePembayaran {
    public static void main(String[] args) throws IOException {
        int sksPerSemester = 24;
        int biayaPerSKS = 75000;
        int dendaPerSKS = 5000;

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Masukkan jumlah mata kuliah yang Anda ambil: ");
        int jumlahSKS = Integer.parseInt(reader.readLine());

        int totalBiayaKuliah = jumlahSKS * biayaPerSKS;

        if (jumlahSKS <= sksPerSemester) {
            System.out.println("Total biaya kuliah Anda: Rp " + totalBiayaKuliah);
        } else {
            int biayaDenda = (jumlahSKS - sksPerSemester) * dendaPerSKS;
            int totalBiayaTermasukDenda = totalBiayaKuliah + biayaDenda;
            System.out.println("Total biaya kuliah Anda (termasuk biaya denda): Rp " + totalBiayaTermasukDenda);
        }
    }
}
